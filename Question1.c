/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/

#include <stdio.h>
/*
 * Given a byte array(data) of length(len) search for the starting position   
 * of 32 bit word 0xDEADBEEF and return the position
 * If it is not found return -1
*/
int search_sync(unsigned char *data, int len)
{
    
    int top= len-1;
    
    while(top!=-1)
    {
     if (data[top]  == 0xef) 
        {
            top= top-1;
              if (data[top]  == 0xbe) 
              {
                  top=top-1;
                  if (data[top]  == 0xad) 
                  {
                      top=top-1;
                     if (data[top]  == 0xde)   
					 return top;
                  }
                  
              }
        }
        
      top=top-1;  
    }
     
    return -1;  
    
 }
    
 

unsigned char d1[] = {   0x12,   0x34,  0x56,   0x78,  0xde, 0xad, 0xbe, 0xef, 0, 0 }; 
unsigned char d2[] = {   0x12,   0x34,  0x56,   0x78,  0xde, 0xad, 0xbe, 0xe0, 0, 0 }; ;


int main(void) 
{
    int pos = -1;
    printf("In Main1\n");
    
    pos = search_sync(d1,10);
    printf("pos=%d\n",pos);
    pos = search_sync(d1,8);
    printf("pos=%d\n",pos);
    pos = search_sync(d2,10);
    printf("pos=%d\n",pos);
    
    return 0;
}