
#include <stdio.h>

//Length of data in between sync bytes
/* 
The function should return the length of data which is between two y's.
Example: y=0xe, array = { 0x1,x2,0xe,0x2,0x3,0x4,0xe). Return 3. 
Return -1 if the pattern is not found;
*/
#include <stdio.h>

char arr[10] = {0x01,0x02,0x0e,0x02,0x03,0x00,0x04,0x0e};


int find(char *a, int len, char y)
{
      int space=-1;
      int flag=0;
      
    for (int index=0;index<len;index++)
    {
        
      if (a[index]== 0x0e)
      {
         flag++;
      }
    
            if (flag==1)
              {
                  space++;
              }
              else if(flag==2)
              {
                return space;
              }
     
    }
    
     return -1;
}


int main()
{
  int Space=0;
 Space=find(arr,8, 0x0e);
  printf("Space =%d\n",Space);
}
